@extends('layout.layout')

@section('title','Статьи')

@section('content')

    <!--about-part start-->
    <section class="about-part blog-part" @isset($postTitle) style="background-image: url({{asset('/storage/'.str_replace('\\', '/', $postTitle->image))}})" @endisset>
        <div class="container">
            <div class="about-part-details text-center">
                <h2>Статьи</h2>
                <div class="about-part-content">
                    <div class="breadcrumbs">
                        <div class="container">
                            <ol class="breadcrumb">
                                {{Breadcrumbs::render('blog')}}
                            </ol><!--/.breadcrumb-->
                        </div><!--/.container-->
                    </div><!--/.breadcrumbs-->
                </div><!--/.about-part-content-->
            </div><!--/.about-part-details-->
        </div><!--/.container-->

    </section><!--/.about-part-->
    <!--about-part end-->

    <!--nwes start -->
    <section  class="news">
        <div class="container">
            <div class="news-details">
                <div class="section-header text-center">
                    <h2>Статьи</h2>
                    <p>
                     @isset($postTitle)
                            {{$postTitle->description}}
                     @endisset
                    </p>
                </div><!--/.section-header-->
                <div class="news-card">
                    <div class="news-card news-card-pb-25">
                        <div class="row">
                            @forelse($posts as $post)
                            <div class="col-md-4 col-sm-6">
                                <div class="single-news-box">
                                    <div class="news-box-bg">
                                        <img src="{{Voyager::image($post->thumbnail('cropped'))}}" alt="blog image">
                                        <div class="isotope-overlay">
                                            <a href="{{route('blog.single',$post->slug)}}">
                                                <span class="lnr lnr-link"></span>
                                            </a>
                                        </div>
                                    </div><!--/.team-box-bg-->
                                    <div class="news-box-inner">
                                        <h3>
                                            <a href="{{route('blog.single',$post->slug)}}">
                                                {{$post->heading}}
                                            </a>
                                        </h3>
                                        <p class="news-meta">
                                            <span>{{ Date::parse($post->created_at)->format('j F Y г.') }}</span>
                                        </p>
                                    </div><!--/.news-box-inner-->
                                </div><!--/.single-news-box-->
                            </div><!--.col-->
                            @empty
                                <p>На данные момент новостей нет</p>
                            @endforelse
                        </div><!--/.row-->
                        <div class="project-btn text-center">
                            {{ $posts->links('vendor.pagination.default') }}
                        </div><!--/.project-btn-->
                    </div><!--/.news-card-->
                    </div><!--/.row-->
                </div><!--/.news-card-->
            </div><!--/news-details-->
        </div><!--/.container-->

    </section><!--/news-->
    <!--news end-->

    <!-- new-project start -->
    <section  class="new-project">
        <div class="container">
            <div class="new-project-details">
                <div class="row">
                    <div class="col-md-10 col-sm-8">
                        <div class="single-new-project">
                            <h3>
                                Задать вопрос, мы вам перезвоним
                            </h3>
                        </div><!-- /.single-new-project-->
                    </div><!-- /.col-->
                    <div class="col-md-2 col-sm-4">
                        <div class="single-new-project">
                            <button class="slide-btn pop-up__contact-btn">
                                Перезвонить
                            </button>
                        </div><!-- /.single-new-project-->
                    </div><!-- /.col-->
                </div><!-- /.row-->
            </div><!-- /.new-project-details-->
        </div><!-- /.container-->

    </section><!-- /.new-project-->
    <!-- new-project end -->

@endsection


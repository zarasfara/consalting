@extends('layout.layout')

@section('title', 'О нас')

@if(isset($aboutSeo))
    @section('meta-title', $aboutSeo->title)
    @section('meta-description', $aboutSeo->description)
    @section('meta-keywords', $aboutSeo->keywords)
@endif

@section('content')
		<!--about-part start-->
		<section class="about-part" style="background: url({{asset('images/about/about-banner.jpg')}})">
			<div class="container">
				<div class="about-part-details text-center">
					<h2>О нас</h2>
					<div class="about-part-content">
						<div class="breadcrumbs">
							<div class="container">
								<ol class="breadcrumb">
                                    {{Breadcrumbs::render('blog')}}
								</ol><!--/.breadcrumb-->
							</div><!--/.container-->
						</div><!--/.breadcrumbs-->
					</div><!--/.about-part-content-->
				</div><!--/.about-part-details-->
			</div><!--/.container-->

		</section><!--/.about-part-->
		<!--about-part end-->

		<!--about-history start-->
		<div class="about-history">
			<div class="container">
				<div class="about-history-content">

					<div class="row">

						<div class="col-md-5 col-sm-12">
							<div class="single-about-history">
								<div class="about-history-img">
                                    @isset($historyBlock)
									<img src="{{asset('storage/'.$historyBlock->image)}}" alt="about">
                                    @endisset
								</div><!--/.about-history-img-->
							</div><!--/.single-about-history-->
						</div><!--/.col-->

						<div class="col-md-offset-1 col-md-6 col-sm-12">
							<div class="single-about-history">
								<div class="about-history-txt">
									<h2>{{$historyBlock->heading ?? 'Наша история'}}</h2>
									<p>
										{{$historyBlock->description ?? 'Наша история - описание'}}
									</p>

									<div class="main-timeline">
                                        @isset($historyTimeStamps)
                                        @foreach($historyTimeStamps as $item)
										<div class="row">
											<div class="col-md-2 col-sm-2">
												<div class="experience-time">
													<h3>{{$item->year}}</h3>
												</div><!--/.experience-time-->
											</div><!--/.col-->
											<div class="col-md-10 col-sm-10">
												<div class="timeline">

													<div class="timeline-content">
														<h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

														<ul class="description">
															<li>{{$item->description}}</li>

														</ul>
													</div><!--/.timeline-content-->
												</div><!--/.timeline-->
											</div><!--/.col-->
										</div><!--/.row-->
                                        @endforeach
                                        @endisset
									</div><!--.main-timeline-->
								</div><!--/.about-history-txt-->
							</div><!--/.single-about-history-->
						</div><!--/.col-->

					</div><!--/.row-->
					<div class="row">

						<div class="about-vission-content">

							<div class="col-md-6 col-sm-12">
								<div class="single-about-history">
									<div class="about-history-txt">
										<h2>{{$visionBlock->heading ?? 'Наши взгляды'}}</h2>
										<p>
											{{$visionBlock->description ?? 'Наши взгляды - описание'}}
										</p>

										<div class="main-timeline  xtra-timeline">
                                            @isset($visionFeature)
                                                @foreach($visionFeature as $item)
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="timeline timeline-ml-20">

                                                            <div class="timeline-content">
                                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                                                                <ul class="description">
                                                                    <li>{{$item->text}}</li>
                                                                </ul>
                                                            </div><!--/.timeline-content-->
                                                        </div><!--/.timeline-->
                                                    </div><!--/.col-->
                                                </div><!--/.row-->
                                                @endforeach
                                            @endisset
										</div><!--.main-timeline-->
									</div><!--/.about-history-txt-->
								</div><!--/.single-about-history-->
							</div><!--/.col-->

							<div class="col-md-offset-1 col-md-5 col-sm-12">
								<div class="single-about-history">
									<div class="about-history-img">
										<img src="{{asset('storage/'.$visionBlock->image)}}" alt="about">
									</div><!--/.about-history-img-->
								</div><!--/.single-about-history-->
							</div><!--/.col-->
						</div><!--/.about-vission-content-->
					</div><!--/.row-->
				</div><!--/.about-history-content-->
			</div><!--/.container-->

		</div><!--/.about-history-->
		<!--about-history end-->


		<!--statistics start-->
		<section  class="statistics">
			<div class="container">
				<div class="statistics-counter">
                    @isset($homeFeatures)
                        @foreach($homeFeatures as $feature)
                            <div class="col-md-3 col-sm-6">
                                <div class="single-ststistics-box">
                                    <div class="statistics-img">
                                        <img src="{{asset('/storage')}}/{{$feature->image}}" alt="counter-icon"/>
                                    </div><!--/.statistics-img-->
                                    <div class="statistics-content">
                                        <div class="counter">{{$feature->days}}</div>
                                        <h3>{{$feature->heading}}</h3>
                                    </div><!--/.statistics-content-->
                                </div><!--/.single-ststistics-box-->
                            </div><!--/.col-->
                        @endforeach
                    @endisset
				</div><!--/.statistics-counter-->
			</div><!--/.container-->

		</section><!--/.statistics-->
		<!--statistics end-->

		<!--we-do start -->
		<section  class="we-do">
			<div class="container">
				<div class="we-do-details">
                    <div class="section-header text-center">
                        @isset($homeWeDo)
                            <h2>{{$homeWeDo->heading}}</h2>
                            <p>
                                {{$homeWeDo->paragraph}}
                            </p>
                        @endisset
                    </div><!--/.section-header-->
                    <div class="we-do-carousel">
                        @isset($homeWeDoCards)
                            @foreach($homeWeDoCards as $card)
                            <div class="col-sm-4 col-xs-12">
                                <div class="single-we-do-box text-center">
                                    <div class="we-do-description">
                                        <div class="we-do-info">
                                            <div class="we-do-img">
                                                <img src="{{asset('/storage')}}/{{$card->image}}"
                                                     alt="image of consultency"/>
                                            </div><!--/.we-do-img-->
                                            <div class="we-do-topics">
                                                <h2>
                                                    <p>
                                                        {{$card->heading}}
                                                    </p>
                                                </h2>
                                            </div><!--/.we-do-topics-->
                                        </div><!--/.we-do-info-->
                                        <div class="we-do-comment">
                                            <p>
                                                {{$card->paragraph}}
                                            </p>
                                        </div><!--/.we-do-comment-->
                                    </div><!--/.we-do-description-->
                                </div><!--/.single-we-do-box-->
                            </div>
                        @endforeach
                        @endisset
                    </div><!--/.we-do-carousel-->
				</div><!--/.we-do-details-->
			</div><!--/.container-->

		</section><!--/.we-do-->
		<!--we-do end-->

		<!-- new-project start -->
		<section  class="new-project">
				<div class="container">
					<div class="new-project-details">
						<div class="row">
							<div class="col-md-10 col-sm-8">
								<div class="single-new-project">
									<h3>
                                        Задать вопрос, мы вам перезвоним
									</h3>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
							<div class="col-md-2 col-sm-4">
								<div class="single-new-project">
									<button class="slide-btn pop-up__contact-btn">
                                        Перезвонить
									</button>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
						</div><!-- /.row-->
					</div><!-- /.new-project-details-->
				</div><!-- /.container-->

		</section><!-- /.new-project-->
		<!-- new-project end -->

@endsection

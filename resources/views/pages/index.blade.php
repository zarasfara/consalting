@extends('layout.layout')

@section('title','Главная')

@isset($homeSeo)
@section('meta-title', $homeSeo->title)
@section('meta-description', $homeSeo->description)
@section('meta-keywords', $homeSeo->keywords)
@endisset

@section('content')

    <!-- header-slider-area start -->
    <section class="header-slider-area">
        <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($homeSlider as $item)
                    <div class="item @if($loop->first) active @endif">
                        <div class="single-slide-item slide-1"
                             style="background-image: url({{asset('/storage/'.str_replace('\\', '/', $item->background))}})">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="single-slide-item-content">
                                            <h2>{{$item->heading}}</h2>
                                            <p>
                                                {!! $item->paragraph !!}
                                            </p>
                                            @isset($item->first_button_text)
                                                <a href="{{url($item->first_button_slug ?? '')}}" class="slide-btn">
                                                    {{$item->first_button_text}}
                                                </a>
                                            @endisset
                                            @isset($item->second_button_text)
                                                <a href="{{url($item->second_button_slug ?? '')}}" class="slide-btn">
                                                    {{$item->second_button_text}}
                                                </a>
                                            @endisset
                                        </div><!-- /.single-slide-item-content-->
                                    </div><!-- /.col-->
                                </div><!-- /.row-->
                            </div><!-- /.container-->
                        </div><!-- /.single-slide-item-->
                    </div><!-- /.item .active-->
                @endforeach
            </div><!-- /.carousel-inner-->

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="lnr lnr-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="lnr lnr-chevron-right"></span>
            </a>
        </div><!-- /.carousel-->

    </section><!-- /.header-slider-area-->
    <!-- header-slider-area end -->
    <!--we-do start -->
    <section class="we-do">
        <div class="container">
            <div class="we-do-details">
                @isset($homeWeDo)
                <div class="section-header text-center">
                    <h2>{{$homeWeDo->heading}}</h2>
                    <p>
                        {{$homeWeDo->paragraph}}
                    </p>
                </div><!--/.section-header-->
                <div class="we-do-carousel">
                    @foreach($homeWeDoCards as $card)
                        <div class="col-sm-4 col-xs-12">
                            <div class="single-we-do-box text-center">
                                <div class="we-do-description">
                                    <div class="we-do-info">
                                        <div class="we-do-img">
                                            <img src="{{asset('/storage')}}/{{$card->image}}"
                                                 alt="image of consultency"/>
                                        </div><!--/.we-do-img-->
                                        <div class="we-do-topics">
                                            <h2>
                                                <p>
                                                    {{$card->heading}}
                                                </p>
                                            </h2>
                                        </div><!--/.we-do-topics-->
                                    </div><!--/.we-do-info-->
                                    <div class="we-do-comment">
                                        <p>
                                            {{$card->paragraph}}
                                        </p>
                                    </div><!--/.we-do-comment-->
                                </div><!--/.we-do-description-->
                            </div><!--/.single-we-do-box-->
                        </div>
                    @endforeach
                </div><!--/.we-do-carousel-->
                @endisset
            </div><!--/.we-do-details-->
        </div><!--/.container-->

    </section><!--/.we-do-->
    <!--we-do end-->

    <!--about-us start -->
    <section class="about-us">
        <div class="container">
            <div class="about-us-content">
                @isset($homeAbout)
                <div class="row">
                    <div class="col-sm-6">
                        <div class="single-about-us">
                            <div class="about-us-txt">
                                <h2>{{$homeAbout->heading}}</h2>
                                <p>
                                    {{$homeAbout->description}}
                                </p>
                                @isset($homeAbout->button_text)
                                    <div class="project-btn">
                                        <a href="{{$homeAbout->button_link}}" class="project-view">
                                            {{$homeAbout->button_text}}
                                        </a>
                                    </div><!--/.project-btn-->
                                @endisset
                            </div><!--/.about-us-txt-->
                        </div><!--/.single-about-us-->
                    </div><!--/.col-->
                    <div class="col-sm-6">
                        <div class="single-about-us">
                            <div class="about-us-img">
                                <img src="{{asset('/storage')}}/{{$homeAbout->image}}" alt="about images">
                            </div><!--/.about-us-img-->
                        </div><!--/.single-about-us-->
                    </div><!--/.col-->
                </div><!--/.row-->
                @endisset
            </div><!--/.about-us-content-->
        </div><!--/.container-->
    </section><!--/.about-us-->
    <!--about-us end -->

    <!--service start-->
    <section class="service">
        <div class="container">
            <div class="service-details">
                <div class="section-header text-center">
                    <h2>Наши услуги</h2>
                    <p>
                        Pallamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
                        in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                </div><!--/.section-header-->
                <div class="service-content-one">
                    <div class="row">
                    @isset($services)
                        @foreach($services as $service)
                        <div class="col-sm-4 col-xs-12">
                            <div class="service-single text-center">
                                <div class="service-img">
                                    <img src="{{asset('images/service/service1.png')}}" alt="image of service"/>
                                </div><!--/.service-img-->
                                <div class="service-txt">
                                    <h2>
                                        <a href="{{route('service',$service->slug)}}">{{$service->heading}}</a>
                                    </h2>
                                    <p>
                                        {{Str::limit($service->paragraph,80,'...')}}
                                    </p>
                                    <a href="{{route('service',$service->slug)}}" class="service-btn">
                                        Подробнее
                                    </a>
                                </div><!--/.service-txt-->
                            </div><!--/.service-single-->
                        </div><!--/.col-->
                        @endforeach
                    @endisset
                    </div><!--/.row-->
                </div><!--/.service-content-one-->
            </div><!--/.service-details-->
        </div><!--/.container-->

    </section><!--/.service-->
    <!--service end-->

    <!--statistics start-->
    <section class="statistics" style="background-image: url({{asset('images/counter/counter-banner.jpg')}})no-repeat">
        <div class="container">
            <div class="statistics-counter ">
            @isset($homeFeatures)
                @foreach($homeFeatures as $feature)
                    <div class="col-md-3 col-sm-6">
                        <div class="single-ststistics-box">
                            <div class="statistics-img">
                                <img src="{{asset('/storage')}}/{{$feature->image}}" alt="counter-icon"/>
                            </div><!--/.statistics-img-->
                            <div class="statistics-content">
                                <div class="counter">{{$feature->days}}</div>
                                <h3>{{$feature->heading}}</h3>
                            </div><!--/.statistics-content-->
                        </div><!--/.single-ststistics-box-->
                    </div><!--/.col-->
                @endforeach
            @endisset
            </div><!--/.statistics-counter-->
        </div><!--/.container-->

    </section><!--/.statistics-->
    <!--statistics end-->

    <!--team start -->
    <section class="team">
        <div class="container">
            <div class="team-details">
                <div class="project-header team-header text-left">
                    @isset($teamText)
                    <h2>{{$teamText->heading}}</h2>
                    <p>
                        {{$teamText->paragraph}}
                    </p>
                    @endisset
                </div><!--/.project-header-->
                <div class="team-card">
                    <div class="container">
                        @isset($teamSlider)
                        <div class="row">
                            <div class="owl-carousel  team-carousel">
                                @foreach($teamSlider as $slider)
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="single-team-box team-box-bg-2"
                                             style="background-image: url({{asset('/storage/'.str_replace('\\', '/', $slider->image))}})">
                                            <div class="team-box-inner">
                                                <h3>{{$slider->name}}</h3>
                                                <p class="team-meta">
                                                    {{$slider->position}}
                                                </p>
                                            </div><!--/.team-box-inner-->
                                        </div><!--/.single-team-box-->
                                    </div><!--.col-->
                                @endforeach
                            </div><!--/.team-carousel-->
                        </div><!--/.row-->
                        @endisset
                    </div><!--/.container-->
                </div><!--/.team-card-->
            </div><!--/.team-details-->
        </div><!--/.container-->

    </section><!--/.team-->
    <!--team end-->
    <section class="clients">
        <div class="container">
            <h2 class="clients__heading">Нам доверяют</h2>
        </div>
        <div class="container">
            <div class="clients-area">
                <div class="owl-carousel client-carousel">
                    @isset($trustSliders)
                        @foreach($trustSliders as $slider)
                            <div class="item cleints__item" data-id="{{$slider->id}}">
                                <div>
                                    <img src="{{asset('storage/'.$slider->image)}}" alt="brand-image"/>
                                    <p class="text-center cleints__item-paragraph">{{$slider->name}}</p>
                                </div>
                            </div><!--/.item-->
                        @endforeach
                    @endisset
                </div><!--/.owl-carousel-->
            </div><!--/.clients-area-->
        </div><!--/.container-->
        <div class="modal-overlay">
            <div class="clients__more">
                <div class="clients__more-close">
                    <img src="{{asset('images/links/close.png')}}" alt="">
                </div>
                <div class="clients__more-content">

                </div>
            </div>
        </div>
    </section><!--/.clients-->
    <!--nwes start -->
    <section class="news">
        <div class="container">
            <div class="news-details">
                <div class="section-header text-center">
                    <h2>Последние новости</h2>
                    <p>
                        {{$postTitle->description}}
                    </p>
                </div><!--/.section-header-->
                <div class="news-card news-card-pb-25">
                    <div class="row">
                        @isset($posts)
                        @foreach($posts as $post)
                        <div class="col-md-4 col-sm-6">
                            <div class="single-news-box">
                                <div class="news-box-bg">
                                    <img src="{{Voyager::image($post->thumbnail('cropped'))}}" alt="blog image">
                                    <div class="isotope-overlay">
                                        <a href="{{route('blog.single',$post->slug)}}">
                                            <span class="lnr lnr-link"></span>
                                        </a>
                                    </div>
                                </div><!--/.team-box-bg-->
                                <div class="news-box-inner">
                                    <h3>
                                        <a href="{{route('blog.single',$post->slug)}}">
                                            {{$post->heading}}
                                        </a>
                                    </h3>
                                    {{Date::parse($post->created_at)->format('j F Y г.')}}
                                     <a href="#" class="learn-btn">
                                        learn morew
                                    </a>
                                </div><!--/.news-box-inner-->
                            </div><!--/.single-news-box-->
                        </div><!--.col-->
                        @endforeach
                        @endisset
                    </div><!--/.row-->
                    <div class="project-btn text-center">
                        <a href="{{route('blog')}}" class="project-view">Подробнее
                        </a>
                    </div><!--/.project-btn-->
                </div><!--/.news-card-->

            </div><!--/news-details-->
        </div><!--/.container-->
    </section><!--/news-->
    <!--news end-->

    <!--contact start-->
    <section id="contact" class="contact" style="background: url({{asset('images/footer/map.jpg')}})">
        <div class="container">
            <div class="contact-details">
                <div class="section-header contact-head  text-center">
                    <h2>Контакты</h2>
                    <p>
                        {{$contactContent->description ?? 'Контакты'}}
                    </p>
                </div><!--/.section-header-->
                <div class="contact-content">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-5">
                            <div class="single-contact-box">
                                <div class="contact-right">
                                    <div class="contact-adress">
                                        <div class="contact-office-address">
                                            @isset($contactContent)
                                            <h3>Контактная информация</h3>
                                            <p>
                                                {{$contactContent->street}}
                                            </p>
                                            <div class="contact-online-address">
                                                <div class="single-online-address">
                                                    <img src="{{asset('images/links/telephone-white.png')}}" alt="links-images">
                                                    {{$contactContent->phone}}
                                                </div><!--/.single-online-address-->

                                                <div class="single-online-address">
                                                    <img src="{{asset('images/links/gmail-white.png')}}" alt="links-images">
                                                    <span>{{$contactContent->email}}</span>
                                                </div><!--/.single-online-address-->
                                            </div><!--/.contact-online-address-->
                                            @endisset
                                        </div><!--/.contact-office-address-->
                                        <div class="contact-office-address">
                                            <h3>Соц. сети</h3>
                                            <div class="contact-icon">
                                                <ul>
                                                    @isset($socialLinks)
                                                        @foreach ($socialLinks as $item)
                                                            <li>
                                                                <a href="{{$item->link}}">
                                                                    <img src="{{asset('storage/'.$item->image)}}" alt="">
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endisset
                                                </ul><!--/ul-->
                                            </div><!--/.contact-icon-->
                                        </div><!--/.contact-office-address-->

                                    </div><!--/.contact-address-->
                                </div><!--/.contact-right-->
                            </div><!--/.single-contact-box-->
                        </div><!--/.col-->
                        <div class="col-sm-5">
                            <div class="single-contact-box">
                                <div class="contact-form">
                                    <h3>Заказать консультацию</h3>
                                    <form id="contact-form" method="post" action="{{route('application')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="full_name" placeholder="Ф.И.О" name="full_name">
                                                    <span class="span_error full_name_error"></span>
                                                </div>
                                            </div><!--/.col-->
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="organization_name"
                                                           placeholder="Организация" name="organization_name">
                                                    <span class="span_error organization_name_error"></span>
                                                </div><!--/.form-group-->
                                            </div><!--/.col-->
                                        </div><!--/.row-->
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" placeholder="Почта" name="email">
                                                </div><!--/.form-group-->
                                            </div><!--/.col-->
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="phone"
                                                           placeholder="Телефон" name="telephone">
                                                </div><!--/.form-group-->
                                            </div><!--/.col-->
                                        </div><!--/.row-->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea name="question" class="form-control" rows="7" id="question"
                                                              placeholder="Вопрос"></textarea>
                                                </div><!--/.form-group-->
                                            </div><!--/.col-->
                                        </div><!--/.row-->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="single-contact-btn pull-right">
                                                    <button class="contact-btn" type="submit">Отправить</button>
                                                </div><!--/.single-single-contact-btn-->
                                            </div><!--/.col-->
                                        </div><!--/.row-->
                                    </form><!--/form-->
                                    <div id="result"></div>
                                </div><!--/.contact-form-->
                            </div><!--/.single-contact-box-->
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div><!--/.contact-content-->
            </div><!--/.contact-details-->
        </div><!--/.container-->
    </section><!--/.contact-->
@endsection

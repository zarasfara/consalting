@extends('layout.layout')

@section('title', 'Услуги')

@section('content')
    <!--about-part start-->
    <section class="about-part service-part" @isset($serviceTitle)style="background-image: url({{asset('storage/'.str_replace('\\', '/', $serviceTitle->image))}})" @endisset>
        <div class="container">
            <div class="about-part-details text-center">
                <h2>Услуги</h2>
                <div class="about-part-content">
                    <div class="breadcrumbs">
                        <div class="container">
                            <ol class="breadcrumb">
                                {{Breadcrumbs::render('services')}}
                            </ol><!--/.breadcrumb-->
                        </div><!--/.container-->
                    </div><!--/.breadcrumbs-->
                </div><!--/.about-part-content-->
            </div><!--/.about-part-details-->
        </div><!--/.container-->

    </section><!--/.about-part-->
    <!--about-part end-->

    <!--service start-->
    <section  class="service">
        <div class="container">
            <div class="service-details">
                <div class="section-header text-center">
                    <h2>Наши услуги</h2>
                    <p>
                        {{$serviceTitle->description ?? 'Описание'}}
                    </p>
                </div><!--/.section-header-->
                <div class="service-content-one">
                    <div class="row">
                        @forelse($services as $service)
                        <div class="col-sm-4 col-xs-12">
                            <div class="service-single text-center">
                                <div class="service-img">
                                    <img src="{{asset('storage/'.$service->image)}}" alt="image of service" />
                                </div><!--/.service-img-->
                                <div class="service-txt">
                                    <h2>
                                        <a href="{{route('service',$service->slug)}}">{{$service->heading}}</a>
                                    </h2>
                                    <p>
                                        {{Str::limit($service->paragraph,80,'...')}}
                                    </p>
                                    <a href="{{route('service',$service->slug)}}" class="service-btn">
                                        Подробнее
                                    </a>
                                </div><!--/.service-txt-->
                            </div><!--/.service-single-->
                        </div><!--/.col-->
                        @empty
                            <p>На данные момент новостей нет</p>
                        @endforelse
                    </div><!--/.row-->
                </div><!--/.service-content-one-->
            </div><!--/.service-details-->
            <div class="project-btn text-center">
                {{ $services->links('vendor.pagination.default') }}
            </div><!--/.project-btn-->
        </div><!--/.container-->
    </section><!--/.service-->
    <!--service end-->

    <!-- new-project start -->
    <section class="new-project">
        <div class="container">
            <div class="new-project-details">
                <div class="row">
                    <div class="col-md-10 col-sm-8">
                        <div class="single-new-project">
                            <h3>
                                Задать вопрос, мы вам перезвоним
                            </h3>
                        </div><!-- /.single-new-project-->
                    </div><!-- /.col-->
                    <div class="col-md-2 col-sm-4">
                        <div class="single-new-project">
                            <button class="slide-btn pop-up__contact-btn">
                                Перезвонить
                            </button>
                        </div><!-- /.single-new-project-->
                    </div><!-- /.col-->
                </div><!-- /.row-->
            </div><!-- /.new-project-details-->
        </div><!-- /.container-->
    </section><!-- /.new-project-->
    <!-- new-project end -->

@endsection

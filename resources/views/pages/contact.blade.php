@extends('layout.layout')

@section('title', 'Контакты')

@section('content')

		<!--contact start-->
		<section  class="contact" style="background-image: url({{asset('images/footer/map.jpg')}})">
			<div class="container">
				<div class="contact-details">
					<div class="section-header contact-head  text-center">
						<h2>Контакты</h2>
						<p>
                            {{$contactContent->description ?? 'Контакты описание'}}
						</p>
					</div><!--/.section-header-->
					<div class="contact-content">
						<div class="row">
							<div class=" col-sm-6">
                                <div style="position:relative;overflow:hidden;">{!! $contactContent->map !!}</div>
							</div><!--/.col-->
							<div class="col-sm-offset-1 col-sm-5">
                                <div class="single-contact-box">
                                    <div class="contact-right">
                                        <div class="contact-adress">
                                            <div class="contact-office-address">
                                                <h3>Контактная информация</h3>
                                                @isset($contactContent)
                                                <p>
                                                    {{$contactContent->street}}
                                                </p>
                                                <div class="contact-online-address">
                                                    <div class="single-online-address">
                                                        <i class="fa fa-phone"></i>
                                                        {{$contactContent->phone}}
                                                    </div><!--/.single-online-address-->

                                                    <div class="single-online-address">
                                                        <i class="fa fa-envelope-o"></i>
                                                        <span>{{$contactContent->email}}</span>
                                                    </div><!--/.single-online-address-->
                                                </div><!--/.contact-online-address-->
                                                @endisset
                                            </div><!--/.contact-office-address-->
                                            <div class="contact-office-address">
                                                <h3>Соц. сети</h3>
                                                <div class="contact-icon">
                                                    <ul>
														@isset($socialLinks)
															@foreach ($socialLinks as $item)
															<li>
																<a href="{{$item->link}}">
																	<img src="{{asset('storage/'.$item->image)}}" alt="">
																</a>
															</li>
															@endforeach
														@endisset
                                                    </ul><!--/ul-->
                                                </div><!--/.contact-icon-->
                                            </div><!--/.contact-office-address-->

                                        </div><!--/.contact-address-->
                                    </div><!--/.contact-right-->
                                </div><!--/.single-contact-box-->
							</div><!--/.col-->
						</div><!--/.row-->
					</div><!--/.contact-content-->
				</div><!--/.contact-details-->
			</div><!--/.container-->

		</section><!--/.contact-->

		<!-- new-project start -->
		<section  class="new-project">
				<div class="container">
					<div class="new-project-details">
						<div class="row">
							<div class="col-md-10 col-sm-8">
								<div class="single-new-project">
									<h3>
                                        Задать вопрос, мы вам перезвоним
									</h3>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
							<div class="col-md-2 col-sm-4">
								<div class="single-new-project">
									<button class="slide-btn pop-up__contact-btn">
										    Перезвонить
									</button>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
						</div><!-- /.row-->
					</div><!-- /.new-project-details-->
				</div><!-- /.container-->

		</section><!-- /.new-project-->
		<!-- new-project end -->

@endsection

@extends('layout.layout')

@section('title',$post->heading)
@section('meta-title', $post->seo_title)
@section('meta-description', $post->seo_description)
@section('meta-keywords', $post->seo_keywords)
@section('content')
		<!--about-part start-->
		<section class="about-part" @isset($postTitle) style="background-image: url({{asset('/storage/'.str_replace('\\', '/', $postTitle->image))}});" @endisset>
			<div class="container">
				<div class="about-part-details text-center">
					<h2>Статьи</h2>
					<div class="about-part-content">
						<div class="breadcrumbs">
							<div class="container">
								<ol class="breadcrumb">
                                    {{Breadcrumbs::render('post',$post)}}
								</ol><!--/.breadcrumb-->
							</div><!--/.container-->
						</div><!--/.breadcrumbs-->
					</div><!--/.about-part-content-->
				</div><!--/.about-part-details-->
			</div><!--/.container-->

		</section><!--/.about-part-->
		<!--about-part end-->

		<!--nwes start -->
		<section  class="news">
			<div class="container">
				<div class="news-details">
					<div class="news-card">
						<div class="row">
							<div class="col-md-5">
								<div class="blog-left">

									<div class="bl-article-post-img">
											<img src="{{asset('storage')}}/{{$post->image}}" alt="image" />
									</div><!--/.bl-article-post-img-->

                                    <div class="bl-single-contact-box">

                                        <ul>

                                            <li>
                                                <div class="bl-list-icon">
                                                    <ul>
                                                        <li>
                                                            Поделиться :
                                                        </li><!-- / li -->
                                                        <li>
                                                            <script type="text/javascript">
                                                                document.write(VK.Share.button({
                                                                        title: `{{$post->heading}}`,
                                                                        image: `{{asset('storage')}}/{{$post->image}}`,
                                                                    },
                                                                    {
                                                                        type: "custom",
                                                                        text: `<img src="{{asset('images/blog/vk.png')}}" alt="twitter">`
                                                                    }
                                                                ));
                                                            </script>
                                                        </li><!-- / li -->
                                                    </ul><!-- / ul -->
                                                </div><!-- /.bl-list-icon -->
                                            </li>
                                        </ul><!--/ul-->
                                    </div><!--/.bl-single-contact-box-->
								</div><!--/.blog-left-->

							</div><!--/.col-->
                            <div class="col-md-7 bl-article-single-txt">

                                <div class="bl-single-head">
                                    <h3>
                                        <a href="#">
                                            {{$post->heading}}
                                        </a>
                                    </h3>
                                    <p>
                                        <span> {{ Date::parse($post->created_at)->format('j F Y г.') }}</span>
                                    </p>
                                </div><!--/.bl-single-head-->

                                <div class="bl-single-para">
                                    {!! $post->paragraph !!}
                                </div><!--/.bl-single-para-->
                            </div><!--/.bl-article-single-txt-->
						</div><!--/.row-->
					</div><!--/.news-card-->
				</div><!--/news-details-->
			</div><!--/.container-->

		</section><!--/news-->
		<!--news end-->
        <section>
            <div class="container">
                <div class="clients-area">
                    <div class="owl-carousel blog-single__slider">
                        @isset($post->slider_images)
                            @foreach(json_decode($post->slider_images) as $slider)
                                <div class="item">
                                    <a class="lightzoom" href="{{asset('storage')}}/{{$slider}}"> <img src="{{ Voyager::image($post->getThumbnail($slider, 'cropped'))}}" alt="brand-image"/></a>
                                </div><!--/.item-->
                            @endforeach
                        @endisset
                    </div><!--/.owl-carousel-->
                </div><!--/.clients-area-->
            </div><!--/.container-->
        </section><!--/.clients-->
		<!-- new-project start -->
		<section  class="new-project">
				<div class="container">
					<div class="new-project-details">
						<div class="row">
							<div class="col-md-10 col-sm-8">
								<div class="single-new-project">
									<h3>
                                        Задать Вопрос, Мы Вам Перезвоним
									</h3>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
							<div class="col-md-2 col-sm-4">
								<div class="single-new-project">
									<button class="slide-btn pop-up__contact-btn">
                                        Перезвонить
									</button>
								</div><!-- /.single-new-project-->
							</div><!-- /.col-->
						</div><!-- /.row-->
					</div><!-- /.new-project-details-->
				</div><!-- /.container-->

		</section><!-- /.new-project-->
		<!-- new-project end -->
    @push('scripts')
        <script type="text/javascript" src="https://vk.com/js/api/share.js?93" charset="windows-1251"></script>
    @endpush
@endsection



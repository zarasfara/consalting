<!doctype html>
<html class="no-js" lang="ru">

<head>
    <!-- META DATA -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="none"/>

    <title>{{config('app.name')}} - @yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="title" content="@yield('meta-title')?? ">
    <meta name="description" content="@yield('meta-description')">
    <meta name="keywords" content="@yield('meta-keywords')">

    @stack('scripts')

     {{-- Google captcha --}}
    {!! htmlScriptTagJsApi() !!}

    @include('components.style')

</head>

<body>

@include('components.header')


@yield('content')


@if(!Route::is('home'))
    @include('components.callback_form')
@endif

@include('components.footer')


@include('components.scripts')

</body>

</html>




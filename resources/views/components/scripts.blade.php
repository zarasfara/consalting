<!-- jaquery link -->

<script src="{{asset('js/jquery.js')}}"></script>

<!--modernizr.min.js-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script src="{{asset('js/jquery.maskedinput.js')}}"></script>

<!--bootstrap.min.js-->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/bootsnav.js')}}"></script>

<!-- for manu -->
<script src="{{asset('js/jquery.hc-sticky.min.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--owl.carousel.js-->
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/waypoints.min.js')}}"></script>

<!--Custom JS-->
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>

<script src="{{asset('js/lightzoom.js')}}"></script>

<script type="text/javascript">jQuery('.lightzoom').lightzoom({speed: 400, viewTitle: true,isOverlayClickClosing: true,imgPadding:0,});</script>

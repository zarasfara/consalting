<div class="pop-up__bg">
    <div class="pop-up__form col-md-4 ">
        <h3 class="text-center">Заказать консультацию</h3>
        <i class="fa fa-times fa-2x pop-up__close" aria-hidden="true"></i>
        <form id="contact-form" action="{{route('application')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control q" placeholder="Ф.И.О" name="full_name">
                    </div><!--/.form-group-->
                </div><!--/.col-->
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control q" placeholder="Организация" name="organization_name">
                    </div><!--/.form-group-->
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <input type="email" class="form-control q" placeholder="Почта" name="email">
                    </div><!--/.form-group-->
                </div><!--/.col-->
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <input id="phone" type="text" class="form-control q" placeholder="Телефон" name="telephone">
                    </div><!--/.form-group-->
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <textarea name="question" style="resize: none;" class="form-control q" rows="7" id="comment" placeholder="Сообщение"></textarea>
                    </div><!--/.form-group-->
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-contact-btn pull-right">
                        <button class="contact-btn btn" type="submit">Отправить</button>
                    </div><!--/.single-single-contact-btn-->
                </div><!--/.col-->
            </div><!--/.row-->
        </form><!--/form-->
        <div id="result"></div>
    </div>
</div>




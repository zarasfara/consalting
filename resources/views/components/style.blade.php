<!--font-family-->

<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">



<!-- for title img -->
<link rel="shortcut icon" type="image/icon" href="{{asset('images/logo/favicon.png')}}"/>

<!--font-awesome.min.css-->
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

<!--linear icon css-->
<link rel="stylesheet" href="{{asset('css/icon-font.min.css')}}">

<!--animate.css-->
<link rel="stylesheet" href="{{asset('css/animate.css')}}">

<!--owl.carousel.css-->
<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
<link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet"/>


<!--bootstrap.min.css-->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

<!-- bootsnav -->
<link href="{{asset('css/bootsnav.css')}}" rel="stylesheet"/>

<!--style.css-->
<link rel="stylesheet" href="{{asset('css/style.css')}}">

<!--responsive.css-->
<link rel="stylesheet" href="{{asset('css/responsive.css')}}">

<link rel="stylesheet" href="{{asset('css/lightzoom.css')}}">

<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Главная', route('home'));
});

Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Новости', route('blog'));
});

Breadcrumbs::for('services', function ($trail) {
    $trail->parent('home');
    $trail->push('Услуги', route('service'));
});

Breadcrumbs::for('service', function ($trail,$service) {
    $trail->parent('services');
    $trail->push($service->heading, route('service',$service->slug));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('О нас', route('about'));
});

Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('blog');
    $trail->push($post->heading, route('blog.single', $post->slug));
});

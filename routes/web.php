<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[HomeController::class,'index'])->name('home');

Route::get('/contact',[HomeController::class,'contacts'])->name('contact');

Route::get('/about',[AboutController::class,'index'])->name('about');

Route::prefix('blog')->group(function () {
    Route::get('/',[PostController::class,'blog'])->name('blog');
    Route::get('/{slug?}',[PostController::class,'blogSingle'])->name('blog.single');
});

Route::prefix('services')->group(function () {
    Route::get('/',[HomeController::class,'services'])->name('services');
    Route::get('/{slug?}',[HomeController::class,'service'])->name('service');
});

Route::get('/slider-detail/{id}',[HomeController::class,'sliderDetail']);

Route::post('/contacts-form/create',[ContactController::class,'contacts'])->name('application');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_sliders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('background');
            $table->string('heading');
            $table->string('paragraph');
            $table->string('first_button_text')->nullable();
            $table->string('first_button_slug')->nullable();
            $table->string('second_button_text')->nullable();
            $table->string('second_button_slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_sliders');
    }
}

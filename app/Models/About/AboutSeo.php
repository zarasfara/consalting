<?php

namespace App\Models\About;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutSeo extends Model
{
    use HasFactory;

    protected $table = 'about_seo';
}

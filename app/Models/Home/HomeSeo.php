<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeSeo extends Model
{
    use HasFactory;

    protected $table = 'home_seo';
}

<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Post extends Model
{
    use HasFactory;
    use Resizable;

    protected $fillable = [
        'heading',
        'paragraph',
        'image',
        'slug',
        'slider_images',
        'seo_title',
        'seo_description',
        'seo_keywords'

    ];
}

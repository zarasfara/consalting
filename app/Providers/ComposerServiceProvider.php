<?php

namespace App\Providers;

use App\Models\Post\Post;
use App\Models\SocialLink;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['components.footer', 'components.header', 'pages.contact', 'pages.index'], function ($view) {
            $view->with('socialLinks', SocialLink::get());
        });

        View::composer('components.footer',function ($view) {
            $view->with('lastNews', Post::orderBy('created_at')->limit(2)->get());
        });
    }
}

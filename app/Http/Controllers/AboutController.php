<?php

namespace App\Http\Controllers;

use App\Models\About\AboutSeo;
use App\Models\About\HistoryBlock;
use App\Models\About\HistoryTimeStamp;
use App\Models\About\VisionBlock;
use App\Models\Home\HomeFeature;
use App\Models\Home\HomeWeDo;
use App\Models\Home\HomeWeDoCard;
use App\Models\About\VisionFeature;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index() {
        $homeWeDo = HomeWeDo::first();
        $homeWeDoCards = HomeWeDoCard::get();
        $homeFeatures = HomeFeature::get();
        $historyBlock = HistoryBlock::first();
        $historyTimeStamps = HistoryTimeStamp::all();
        $visionBlock = VisionBlock::first();
        $visionFeature = VisionFeature::all();
        $aboutSeo = AboutSeo::first();

        $data = [
            'homeFeatures' => $homeFeatures,
            'homeWeDoCards'=> $homeWeDoCards,
            'homeWeDo' => $homeWeDo,
            'historyBlock' => $historyBlock,
            'historyTimeStamps' => $historyTimeStamps,
            'visionBlock' => $visionBlock,
            'visionFeature' => $visionFeature,
            'aboutSeo' => $aboutSeo,
        ];
        return view('pages.about', ($data));
    }
}

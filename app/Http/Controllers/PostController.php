<?php

namespace App\Http\Controllers;

use App\Models\Post\PostTitle;
use Illuminate\Http\Request;
use App\Models\Post\Post;

class PostController extends Controller
{
    public function blog() {
        $posts = Post::paginate(9);
        $postTitle = PostTitle::first();

        return view('pages.blog',compact('posts','postTitle'));
    }

    public function blogSingle($slug) {
        $post = Post::where('slug',$slug)->firstOrFail();
        $postTitle = PostTitle::first();
        return view('pages.blog_single',compact('post','postTitle'));
    }
}

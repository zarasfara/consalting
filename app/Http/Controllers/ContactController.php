<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contacts(Request $request) {

        $data = $request->validate([

            'full_name'=>'required',
            'organization_name'=>'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'question'=>'nullable',
       ]);

        Contact::create($data);

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Contact\BlockContact;
use App\Models\Home\HomeAbout;
use App\Models\Home\HomeFeature;
use App\Models\Home\HomeSeo;
use App\Models\Home\HomeSlider;
use App\Models\Home\HomeTeamSlider;
use App\Models\Home\HomeTeamText;
use App\Models\Home\HomeTrustSlider;
use App\Models\Home\HomeWeDo;
use App\Models\Home\HomeWeDoCard;
use App\Models\Post\Post;
use App\Models\Post\PostTitle;
use App\Models\Service\Service;
use App\Models\Service\ServiceTitle;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        $homeSlider = HomeSlider::get();
        $homeWeDo = HomeWeDo::first();
        $homeWeDoCards = HomeWeDoCard::get();
        $homeAbout = HomeAbout::first();
        $homeFeatures = HomeFeature::get();
        $trustSliders = HomeTrustSlider::get();
        $teamText = HomeTeamText::first();
        $teamSlider = HomeTeamSlider::get();
        $postTitle = PostTitle::first();
        $homeSeo = HomeSeo::first();
        $posts = Post::latest()->limit(3)->get();
        $contactContent = BlockContact::first();
        $services = Service::latest()->limit(3)->get();

        $data = [
            'homeSlider' => $homeSlider,
            'homeWeDo' => $homeWeDo,
            'homeWeDoCards'=> $homeWeDoCards,
            'homeAbout' => $homeAbout,
            'homeFeatures' => $homeFeatures,
            'trustSliders' => $trustSliders,
            'teamText' => $teamText,
            'teamSlider' => $teamSlider,
            'homeSeo' => $homeSeo,
            'posts' => $posts,
            'contactContent' => $contactContent,
            'services' => $services,
            'postTitle' => $postTitle
        ];

        return view('pages.index',($data));

    }

    public function sliderDetail($id) {
        return HomeTrustSlider::findOrFail($id);
    }

    public function contacts() {
        $contactContent = BlockContact::first();
        return view('pages.contact',compact('contactContent'));
    }

    public function services() {
        $services = Service::paginate(9);
        $serviceTitle = ServiceTitle::first();
        return view('pages.services',compact('services','serviceTitle'));
    }

    public function service($slug) {
        $serviceTitle = ServiceTitle::first();
        $service = Service::where('slug',$slug)->firstOrFail();
        return view('pages.service',compact('service','serviceTitle'));
    }

    public function trust() {
        return view('pages.trust');
    }

}

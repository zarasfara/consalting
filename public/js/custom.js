$(document).ready(function(){
	"use strict";


        /*=========== TABLE OF CONTENTS ===========
        1. Scroll To Top
        2. hcsticky
        3. Counter
        4. owl carousel
        5. vedio player
        6. animation support
        ======================================*/

    // 1. Scroll To Top
		$(window).on('scroll',function () {
			if ($(this).scrollTop() > 600) {
				$('.return-to-top').fadeIn();
			} else {
				$('.return-to-top').fadeOut();
			}
		});
		$('.return-to-top').on('click',function(){
				$('html, body').animate({
				scrollTop: 0
			}, 1500);
			return false;
		});

        jQuery.event.special.touchstart = {
            setup: function( _, ns, handle ) {
                this.addEventListener("touchstart", handle, { passive: !ns.includes("noPreventDefault") });
            }
        };

	// 2 . hcsticky

		$('#menu').hcSticky();


	// 3. counter
		$(window).on('load', function(){
			$('.counter').counterUp({
				delay: 10,
				time: 3000
			});
		});


	// 4. owl carousel

		// i. .team-carousel


		var owl=$('.team-carousel');
		owl.owlCarousel({
			items:4,
			margin:0,

			loop:false,
			autoplay:true,
			smartSpeed:500,

			nav:true,
			navText:["<i class='team-slider__prev fa fa-3x fa-angle-left'></i>","<i class=' team-slider__next fa fa-3x fa-angle-right'></i>"],

			dots:false,
			autoplayHoverPause:true,

			responsiveClass:true,
				responsive:{
					0:{
						items:1
					},
					640:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					}
				}


		});

		// ii. .client (carousel)

		$('.client-carousel').owlCarousel({
			items:4,

			loop:false,
			smartSpeed: 1000,
			autoplay:true,

			nav:true,
            navText:["<i class='client_slider-prev fa fa-3x fa-angle-left'></i>","<i class='client_slider-next fa fa-3x fa-angle-right'></i>"],
            dots:false,

			autoplayHoverPause:true,
			responsive:{
					320:{
						items:1
					},
					415:{
						items:2
					},
					600:{
						items:3
					},
					1000:{
						items:4
					}
				}
			});

    $('.blog-single__slider').owlCarousel({
        items:4,

        loop:false,
        smartSpeed: 1000,
        autoplay:true,

        nav:true,
        navText:["<i class='client_slider-prev fa fa-3x fa-angle-left blog-single__slider-prev'></i>","<i class='client_slider-next fa fa-3x fa-angle-right blog-single__slider-next'></i>"],
        dots:false,

        autoplayHoverPause:true,
        responsive:{
            320:{
                items:1
            },
            415:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });


			$('.play').on('click',function(){
				owl.trigger('play.owl.autoplay',[1000])
			})
			$('.stop').on('click',function(){
				owl.trigger('stop.owl.autoplay')
			})

		// iii.  testimonial

		$("#testemonial-carousel").owlCarousel({
			items: 1,
			autoplay: true,
			loop: true,
			dots:true,
			mouseDrag:true,
			nav:false,
			smartSpeed:1000,
			transitionStyle:"fade",
			animateIn: 'fadeIn',
			animateOut: 'fadeOutLeft'
			// navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});

	// 6. animation support

        $(window).load(function(){

            $(".single-slide-item-content h2, .single-slide-item-content p").removeClass("animated fadeInUp").css({'opacity':'0'});
            $(".single-slide-item-content button").removeClass("animated fadeInLeft").css({'opacity':'0'});
        });

        $(window).load(function(){

            $(".single-slide-item-content h2, .single-slide-item-content p").addClass("animated fadeInUp").css({'opacity':'0'});
            $(".single-slide-item-content button").addClass("animated fadeInLeft").css({'opacity':'0'});

        });

    $('.cleints__item').click(function() {

        $('.clients__more').addClass('active')

        $('.modal-overlay').addClass('active')

        $('body').addClass('fixed')

        const id = $(this).attr('data-id');
        $.get({

            url: 'slider-detail/'+id,
            data: {
                "id": id
            }
        }).done(function (data) {
            $('.clients__more-content').html(data.description);
        })
    })


    $(".clients__more-close").click(function () {

        $('body').removeClass('fixed')

        $(".clients__more").removeClass('active')

        $('.modal-overlay').removeClass('active')

        $('.clients__more-content').text('')

    })

    $('.pop-up__contact-btn').on( "click", function () {
        $('.pop-up__bg').addClass('active');
    })

    $('.pop-up__close').on('click', function () {
        $('.pop-up__bg').removeClass('active');
    })

    $("#phone").mask("+7 (999) 999-99-99");

    $('#contact-form').on('submit', function (e) {
        e.preventDefault()
        let formData = new FormData($('#contact-form')[0]);

        $.ajax({
            type:'post',
            url:$(this).attr('action'),
            // dataType: "json",
            contentType: false,
            processData: false,
            data:formData,
            beforeSend: function () {
                $(document).find('span.error-text').text('')
            },
            success:function(){
                $("#contact-form").find("input[type=text],input[type=email], textarea").val("");

                $('#result').html(`<div class="alert alert-success" role="alert">Форма успешно отправлена</div>`);

                const down = () => {

                    $("#result").animate({
                            height: "0px",
                        }, 600, function() {
                            $(this).remove();
                        }
                    );
                }
                setInterval(down,2000)

            },
            error: (data) => {
                const  message = Object.values(data.responseJSON.errors).join('</br>')
                $('#result').html(`<div class="alert alert-danger">${message}</div>`)
            }

        })
    })

});

